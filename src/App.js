import React, {Fragment} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';
import Landing from "./Screens/Landing";
import TravelHistory from "./Screens/Traveller/TravelHistory";
import HistoryOfExposure from "./Screens/Traveller/HistoryOfExposure";
import HistoryOfExposureA from "./Screens/Traveller/HistoryOfExposureA";
import HistoryOfExposureB from "./Screens/Traveller/HistoryOfExposureB";
import HistoryOfExposureC from "./Screens/Traveller/HistoryOfExposureC";
import NotPUInorPUM from "./Screens/NotPUInorPUM";
import PUI from "./Screens/PUI";
import PUM from "./Screens/PUM";
import TSymptoms from "./Screens/Traveller/TSymptoms";
import RespiratoryIllnesses from "./Screens/Patient/RespiratoryIllnesses";
import Travelled from "./Screens/Patient/Travelled";
import InContact from "./Screens/Patient/InContact";
import PSymptoms from "./Screens/Patient/PSymptoms";
import Ili from "./Screens/Patient/ILI";
import ClinicalPresentation from "./Screens/Patient/ClinicalPresentation";
import './App.css';

 
function App() {
  return (
    <div className="App">
     <Router>
      <Switch>
      <Route path="/PSymptoms">
           <PSymptoms/>
        </Route>
      <Route path="/ILI">
           <Ili/>
        </Route>
        <Route path="/ClinicalPresentation">
           <ClinicalPresentation/>
        </Route>
      <Route path="/InContact">
           <InContact/>
        </Route>
        <Route path="/Travelled">
           <Travelled/>
        </Route>
      <Route path="/RespiratoryIllnesses">
           <RespiratoryIllnesses/>
        </Route>
      <Route path="/PUI">
           <PUI/>
        </Route>
      <Route path="/PUM">
           <PUM/>
        </Route>
      <Route path="/TSymptoms">
           <TSymptoms/>
        </Route>
      <Route path="/NotPUInorPUM">
           <NotPUInorPUM/>
        </Route>
      <Route path="/HistoryOfExposureC">
           <HistoryOfExposureC/>
        </Route>
      <Route path="/HistoryOfExposureB">
           <HistoryOfExposureB/>
        </Route>
      <Route path="/HistoryOfExposureA">
           <HistoryOfExposureA/>
        </Route>
      <Route path="/HistoryOfExposure">
           <HistoryOfExposure/>
        </Route>
        <Route path="/TravelHistory">
           <TravelHistory/>
        </Route>
        <Route path="/">
           <Landing/>
        </Route>
      </Switch>
    </Router>
    </div>
  );
}

export default App;
