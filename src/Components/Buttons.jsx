import React, {Fragment} from 'react';
import {Link, useHistory} from "react-router-dom";

const Buttons= ({question, cancel_done_btn})=>{
        let history = useHistory();
        return (
            <Fragment>
                <div className="d-flex justify-content-between mb-3">
                    <div>
                        <Link className="font-weight-bold" onClick={history.goBack}>&#60;Back</Link>
                    </div>
                    <div>
                        <Link className="font-weight-bold" to="/">Cancel</Link>
                    </div>
                </div>
            </Fragment>
        )
};

export default Buttons;