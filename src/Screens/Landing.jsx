import React from 'react';
import { Link } from 'react-router-dom';

const Landing = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <h1>Assessment of Patients with Possible COVID-19 Infection:</h1>

                <hr className="my-4"></hr>
                <p>*Recommendations provided by this tool do not constitute medical advice and should not be used to diagnose or treat medical conditions.</p>
                <p>Disclaimer: This is not an official tester and was only based on the flowchart given through DOH.</p>
                <div className="mb-3">
                    <Link to="/TravelHistory" className="btn btn-primary btn-md px-5">Traveller?</Link>
                    <Link to="/RespiratoryIllnesses" className="btn btn-primary btn-md px-5 ml-5">Patient?</Link>
                </div>
            </div>
    );
}

export default Landing;
