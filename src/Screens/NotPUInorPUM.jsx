import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../Components/Buttons';

const NotPUInorPUM = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h1>Not PUI nor PUM</h1>
                <hr className="my-4"></hr>
                <h5>
                    <ul className="text-justify col-10 offset-1 ">
                        <li className="mb-3">Follow the Quarantine Protocols.</li>
                        <li className="mb-3">You should practice physical distancing. When outside the home, stay at least six feet away from other people and avoid groups also.</li>
                        <li className="mb-5">In addition, you should wear a cloth face mask when leaving the house. Clean your hands often and avoid touching your face.</li>
                    </ul>
                </h5>
                <div>
                    <Link to="/" className="btn btn-primary btn-lg px-5">Done</Link>
                </div>
            </div>
    );
}

export default NotPUInorPUM;