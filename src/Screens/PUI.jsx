import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../Components/Buttons';

const PUI = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h1>Testing Recommended</h1>
                <hr className="my-4"></hr>
                <h5>
                    <ul className="text-justify col-10 offset-1 ">
                        <li className="mb-3">You should get in touch with a medical professional or your state health department about getting tested for COVID‑19. Access to testing may vary by location and provider.</li>
                        <li className="mb-3">Please admit to designated COVID‑19 isolation area.</li>
                    </ul>
                </h5>
                <div>
                    <Link to="/" className="btn btn-primary btn-lg px-5">Done</Link>
                </div>
            </div>
    );
}

export default PUI;