import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../Components/Buttons';

const PUM = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h1>Not PUI nor PUM</h1>
                <hr className="my-4"></hr>
                <h5>
                    <ul className="text-justify col-10 offset-1 ">
                        <li className="mb-3"><strong>ASYMPTOMATIC patients with appropriate EXPOSURE history</strong> should undergo <strong>home quarantine for 14 days</strong> to monitor for the development of symptomps.</li>
                        <li className="mb-3">You should limit your contact with others including those in your home. If you can, have a room and bathroom that’s just for you.</li>
                        <li className="mb-5">You should eat well, drink fluids, and get plenty of rest. Monitor your symptoms. If they get significantly worse, contact a doctor or a medical professional.</li>
                    </ul>
                </h5>
                <div>
                    <Link to="/" className="btn btn-primary btn-lg px-5">Done</Link>
                </div>
            </div>
    );
}

export default PUM;