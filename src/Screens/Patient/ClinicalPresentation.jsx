import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../../Components/Buttons';

const ClinicalPresentation = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h4>Does the patient have a severe acute respiratory infection or atypical pneumonia AND requiring hospitalization AND with no other etiology to fully explain the clinical presentation, regardless of exposure history?</h4>
                <hr className="my-4"></hr>
                <div>
                    <Link to="/PUI" className="btn btn-primary btn px-5">Yes</Link>
                    <Link to="/ILI" className="btn btn-primary btn px-5 ml-5">No</Link>
                </div>
            </div>
    );
}

export default ClinicalPresentation;