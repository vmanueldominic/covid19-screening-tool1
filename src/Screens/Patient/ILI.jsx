import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../../Components/Buttons';

const Ili = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h4>Was the patien in any cluster of Influenza-like illness (ILI) cases in household or workplace??</h4>
                <hr className="my-4"></hr>
                <div>
                    <Link to="/PUI" className="btn btn-primary btn-lg px-5">Yes</Link>
                    <Link to="/NotPUInorPUM" className="btn btn-primary btn-lg px-5 ml-5">No</Link>
                </div>
            </div>
    );
}

export default Ili;