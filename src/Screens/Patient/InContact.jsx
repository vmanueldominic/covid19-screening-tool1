import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../../Components/Buttons';

const InContact = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h4>CLOSE CONTACT with a confirmed COVID-19 case**(any of the following)</h4>
                <hr className="my-4"></hr>
                <h5>
                    <ul className="text-justify col-10 offset-1 ">
                        <li className="mb-3">Providing direct care without proper PPE*** to confirmed COVID-19 paitent.</li>
                        <li className="mb-3">Staying in the same cose environment (incl. workplace, classroom, household, gatherings).</li>
                        <li className="mb-5">Travaling together in close proximity (1 meter or 3 feet) in any kind of conveyance</li>
                    </ul>
                </h5>
                <div>
                    <Link to="/PSymptoms" className="btn btn-primary btn-lg px-5">Yes</Link>
                    <Link to="/NotPUInorPUM" className="btn btn-primary btn-lg px-5 ml-5">No</Link>
                </div>
            </div>
    );
}

export default InContact;