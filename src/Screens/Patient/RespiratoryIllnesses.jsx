import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../../Components/Buttons';

const RespiratoryIllnesses = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h4>Does the Patient experience any of the following?(Acute Respiratory Illnesses)</h4>
                <hr className="my-4"></hr>
                <h5>
                    <ul className="text-justify col-10 offset-2 ">
                        <li className="mb-3">Fever (≥ 38.0°C).</li>
                        <li className="mb-3">Cough <strong>OR</strong> shortness of Breathing <strong>OR</strong> orther respiratory symptoms.</li>
                        <li className="mb-5">Diarrhea</li>
                    </ul>
                </h5>
                <div>
                    <Link to="/Travelled" className="btn btn-primary btn-lg px-5">Yes</Link>
                    <Link to="/NotPUInorPUM" className="btn btn-primary btn-lg px-5 ml-5">No</Link>
                </div>
            </div>
    );
}

export default RespiratoryIllnesses;