import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../../Components/Buttons';

const Travelled = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h4>Did the patient travelled to or was a residence in a country/area reporting local transmiision COVID-19?(see WHO situation reports for an up-to-date list of countries)</h4>
                <hr className="my-4"></hr>
                <div className="mb-3">
                    <Link to="/PSymptoms" className="btn btn-primary btn-lg px-5">Yes</Link>
                    <Link to="/InContact" className="btn btn-primary btn-lg px-5 ml-5">No</Link>
                </div>
            </div>
    );
}

export default Travelled;