import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../../Components/Buttons';

const HistoryOfExposure = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h4>Did the patient provided any direct care for a COVID-19 patient?</h4>
                <hr className="my-4"></hr>
                <div className="mb-3">
                    <Link to="/TSymptoms" className="btn btn-primary btn-lg px-5">Yes</Link>
                    <Link to="/HistoryOfExposureA" className="btn btn-primary btn-lg px-5 ml-5">No</Link>
                </div>
            </div>
    );
}

export default HistoryOfExposure;