import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../../Components/Buttons';

const HistoryOfExposureA = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h4>Has the patient experienced working together or staying in the same close environment of COVID-19?</h4>
                <hr className="my-4"></hr>
                <div className="mb-3">
                    <Link to="/TSymptoms" className="btn btn-primary btn-lg px-5">Yes</Link>
                    <Link to="/HistoryOfExposureB" className="btn btn-primary btn-lg px-5 ml-5">No</Link>
                </div>
            </div>
    );
}

export default HistoryOfExposureA;