import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../../Components/Buttons';

const HistoryOfExposureC = () =>{
    return(
            <div className="jumbotron col-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h1>Has the patient lived in the same household as a COVID-19 patient within a 14-day period?</h1>
                <hr className="my-4"></hr>
                <div className="mb-3">
                    <Link to="/TSymptoms" className="btn btn-primary btn-lg px-5">Yes</Link>
                    <Link to="/NotPUInorPUM" className="btn btn-primary btn-lg px-5 ml-5">No</Link>
                </div>
            </div>
    );
}

export default HistoryOfExposureC;