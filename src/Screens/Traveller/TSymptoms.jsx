import React from 'react';
import { Link } from 'react-router-dom';
import Buttons from '../../Components/Buttons';

const TSymptoms = () =>{
    return(
            <div className="jumbotron col-sm-10 col-md-6 mx-auto mt-5 border border-info text-center">
                <Buttons/>
                <h4>Does the patient have a fever (≥ 38.0°C) <strong>AND/OR</strong> any respiratory illness(cough and/or colds)?</h4>
                <hr className="my-4"></hr>
                <div className="bt-4">
                    <Link to="/PUI" className="btn btn-primary btn-lg px-5 ">Yes</Link>
                    <Link to="/PUM" className="btn btn-primary btn-lg px-5 ml-md-5">No</Link>
                </div>
            </div>
    );
}

export default TSymptoms;